# Author: Aman Kaushal
# Roll No: IPG2011-016 
#STIS-ASSIGNMENT 1

import sklearn
from sklearn.externals import joblib
import pickle
from sklearn import svm
from sklearn.svm import SVC
import pybrain
from pybrain.datasets import SupervisedDataSet
from pybrain.supervised.trainers import BackpropTrainer
from pybrain.tools.shortcuts import buildNetwork
import os
import os.path
import re
import numpy as np
import csv
import xlrd
import math
import random

ds = SupervisedDataSet(14,50)

tf=open('datasets.csv','r')

for line in tf.readlines():
    data = [float(x) for x in line.strip().split(',') if x != '']
    indata =  tuple(data[:14])
    outdata = tuple(data[14:])
    ds.addSample(indata,outdata)
print ds.indim
print ds.outdim

n = buildNetwork(ds.indim,14,14,ds.outdim,recurrent=True)

#back Proagation Algorithm
t = BackpropTrainer(n,learningrate=0.01,momentum=0.5,verbose=True)
t.trainOnDataset(ds,10)
t.testOnData(verbose=True)
