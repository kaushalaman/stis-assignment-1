# Roll No :IPG2011-016
# Aman Kaushal

import sklearn
from sklearn.externals import joblib
import pickle
from sklearn import svm
from sklearn.svm import SVC
import pybrain
from pybrain.supervised.trainers import BackpropTrainer
from pybrain.datasets import SupervisedDataSet
from pybrain.structure import TanhLayer
from pybrain.structure import SoftmaxLayer
from pybrain.tools.shortcuts import buildNetwork
from multiprocessing import Pool
import os
import os.path
import re
import numpy as np
import csv
import xlrd
import math
import random

net=buildNetwork(14,15,50,bias=True,hiddenclass=TanhLayer)
ds=SupervisedDataSet(14,50)

all_inputs = []
all_targets = []
workbook=xlrd.open_workbook('d_set_all_50_centerofmass.xls',on_demand= True)
worksheet=workbook.sheet_by_index(0)
row=0
column=16

while row<300:
	inc=0
	temp=[] 
	print "iterating through each row"
	while inc<14:
		temp.append(worksheet.cell(row,column+inc).value)
		inc+=1
	all_inputs.append(temp)
	row+=1

row=301
column=16

while row<601:
	inc=0
	temp=[] 
	print "iterating through each row"
	while inc<50:
		temp.append(worksheet.cell(row,column+inc).value)
		inc+=1
	all_targets.append(temp)
	row+=1

for i in range(len(all_inputs)):
	ds.appendLinked(all_inputs[i],all_targets[i])		
	print all_inputs[i]
	print 
	print all_targets[i]
	print

print ds['input']
print ds['target']

print "preparing to train the network"
trainer=BackpropTrainer(net,ds,learningrate=0.01,lrdecay=1,momentum=0.0,verbose=False,batchlearning=False,weightdecay=0.0)
error=10
iteration=0
while error>0.01:
	error=trainer.train()
	iteration+=1
	print "Iteration: {0} Error {1}".format(iteration, error)

joblib.dump(net,'train_network_backprop.pkl')